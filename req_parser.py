import json

file = open("requirements.txt")
packages = {"packages":[]}

for x in file:
    line = x.split("==")
    pkg = {}
    pkg["name"] = line[0]
    pkg["version"] = line[1].strip()
    packages["packages"].append(pkg)

with open('packages.json', 'w') as f:
    json.dump(packages, f)