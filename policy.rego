package example

default allow := false                             

allow := true {                                     
    count(violation) == 0                           
}

violation[pkg] {                              
    pkg := input.packages[_]
    pkg.name == "Flask" 
    pkg.version == "2.0.1"                  
}